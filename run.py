#!/usr/bin/env python
# encoding: utf-8

from app import app, api
from app.resources.resources import *

""" Define API endpoints """
api.add_resource(SubjectListResource, '/subjects/')
api.add_resource(SubjectResource, '/subjects/<int:id>')
api.add_resource(SemesterListResource, '/semesters/')
api.add_resource(SemesterResource, '/semesters/<int:id>')
api.add_resource(CourseListResource, '/courses/')
api.add_resource(CourseResource, '/course/<int:id>')
api.add_resource(TeacherListResource, '/teachers/')
api.add_resource(TeacherResource, '/teacher/<int:id>')
api.add_resource(StudentListResource, '/students/')
api.add_resource(StudentResource, '/students/<int:id>')


if __name__ == "__main__":
    app.run()

