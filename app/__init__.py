from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_restful import Api
import os

""" App, api and database initialization """

app = Flask(__name__)

# Import settings from config.py using environment variables
app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# ORM for database
db = SQLAlchemy(app)

# Serializer
ma = Marshmallow(app)

# Create tables
@app.before_first_request
def create_table():
    db.create_all()

# API in <siteurl>/api
api = Api(app=app, prefix="/api")

from . import views

