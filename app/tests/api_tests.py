import unittest
import json
import os

import requests

from app.tests import BaseCase

class SubjectListTest(BaseCase):

    def test_list_subjects(self):
        response = requests.get(
                self.path+'/api/subjects/'
                )

        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.json()[0])
        self.assertIn("title", response.json()[0])

    def test_add_subject(self):
        payload = json.dumps({
            "title": "English I",
            })

        response = requests.post(
                self.path+'/api/subjects/',
                headers={"Content-type": "application/json"},
                data=payload
                )

        self.assertEqual(str, type(response.json()['title']))
        self.assertEqual(response.status_code, 200)


class SubjectTest(BaseCase):
    def test_view_subject(self):
        response = requests.get(
                self.path+'/api/subjects/'
                )

        subject_id = response.json()[0]['id']
        response = requests.get(
                self.path+'/api/subjects/' + str(subject_id)
                )

        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.json())
        self.assertIn("title", response.json())

    def test_edit_subject(self):
        payload = json.dumps({
            'title': 'German I'
            })

        response = requests.get(
                self.path+'/api/subjects/'
                )
        subject_id = response.json()[0]['id']
        response = requests.put(
                self.path+'/api/subjects/'+str(subject_id),
                headers={"Content-type": "application/json"},
                data=payload
                )

        self.assertEqual(response.status_code, 200)
        self.assertEqual("German I", response.json()["title"])

    def test_remove_subject(self):
        response = requests.get(
                self.path+'/api/subjects/'
                )
        subject_id = response.json()[0]['id']
        response = requests.delete(
                self.path+'/api/subjects/'+str(subject_id),
                )
        self.assertEqual(response.status_code, 204)


class SemesterListTest(BaseCase):

    def test_add_teacher(self):
        payload = json.dumps({
            "first_name": "joao",
            "last_name": "silva",
            "email_address": "joao@example.com",
            "rg": "000000000",
            "cpf": "00000000000",
            "phone_prefix": "00",
            "phone_number": "000000000",
        })


        response = requests.post(
                self.path+'/api/teachers/',
                headers={"Content-type": "application/json"},
                data=payload
                )

        self.assertEqual(response.status_code, 200)

    def test_add_student(self):
        payload = json.dumps({
            "first_name": "maria",
            "last_name": "silva",
            "email_address": "maria@example.com",
            "rg": "000000001",
            "cpf": "00000000001",
            "phone_prefix": "01",
            "phone_number": "000000001",
        })


        response = requests.post(
                self.path+'/api/students/',
                headers={"Content-type": "application/json"},
                data=payload
                )

        self.assertEqual(response.status_code, 200)

    def test_add_semester(self):
        payload = json.dumps({
            "code": "2s2019",
            "current": "False",
        })


        response = requests.post(
                self.path+'/api/semesters/',
                headers={"Content-type": "application/json"},
                data=payload
                )

        self.assertEqual(response.status_code, 200)

    def test_add_course(self):

        subject_id=1
        semester_id=1
        teacher_id=1

        payload = json.dumps({
            "title": "A",
            "subject_id": subject_id,
            "semester_id": semester_id,
            "teacher_id": teacher_id,
        })


        response = requests.post(
                self.path+'/api/courses/',
                headers={"Content-type": "application/json"},
                data=payload
                )

        print(response.json())
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
