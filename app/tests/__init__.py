import unittest
import os
import json

from app import app, db
from app.models import Subject, Student, Teacher, Semester, Course
from app.schemas import *

class BaseCase(unittest.TestCase):

    def setUp(self):
        app.config.from_object(os.environ['APP_TEST_SETTINGS'])
        self.app = app.test_client()
        db.drop_all()
        db.create_all()
        self.db = db
        self.path = 'http://127.0.0.1:5000'

        student = Student(
                id=1,
                first_name="rafael",
                last_name="goncalves",
                email_address="rafael@example.com",
                rg="000000007",
                cpf="00000000007",
                phone_prefix="07",
                phone_number="000000007"
                )

        teacher = Teacher(
                id=1,
                first_name="luiza",
                last_name="barros",
                email_address="luiza@example.com",
                rg="000000006",
                cpf="00000000006",
                phone_prefix="06",
                phone_number="000000006"
                )

        subject = Subject(
                id=1,
                title="Spanish I",
                )

        semester = Semester(
                id=1,
                code="2s2020",
                current=True,
                )

        course = Course(
                id=1,
                title="A",
                subject_id=1,
                subject=subject,
                semester_id=1,
                semester=semester,
                teacher_id=1,
                teacher=teacher,
                )

        course.students.append(student)

        self.db.session.add(subject)
        self.db.session.add(student)
        self.db.session.add(semester)
        self.db.session.add(teacher)
        self.db.session.add(course)

        self.db.session.commit()

        self.assertEqual(app.debug, False)

    def tearDown(self):
        self.db.session.remove()
        self.db.drop_all()

