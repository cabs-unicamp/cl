import requests
from flask import request, render_template, url_for, redirect, jsonify, make_response
from app.models import Subject, Course, Semester, Teacher, Student

from app import app, db

@app.route('/', methods=["GET", "POST"])
def home():
    if request.form:
        title = request.form.get("title")
        subject = Subject(title=title)
        db.session.add(subject)
        db.session.commit()
        return redirect(url_for('add_subject'))
    else:
        subjects = Subject.query.all()
        return render_template("create.html", subjects=subjects)

@app.route('/dashboard', methods=['GET'])
def dashboard():
    subjects = Subject.query.all()
    students = Student.query.all()
    teachers = Teacher.query.all()
    classes = Course.query.all()
    return render_template('dashboard.html', title='Dashboard',
                           students=students, teachers=teachers,
                           subjects=subjects)


@app.route('/add-subject', methods=['GET', 'POST'])
def add_subject():
    if request.method == "POST":
        data = request.get_json()
        response = requests.post(path + "/api/students",
                                 data=data,
                                 headers=get_token())
        output = json.loads(response.text)
        if output.get("error"):
            flash(output["error"], "error")
        else:
            if "error" in output["message"].lower():
                flash(output["message"], "error")
            else:
                flash(output["message"], "success")

    return redirect(url_for('dashboard'))


@app.route("/update", methods=["POST"])
def update_subject():
    newtitle = request.form.get("newtitle")
    oldtitle = request.form.get("oldtitle")
    subject = Subject.query.filter_by(title=oldtitle).first()
    subject.title = newtitle
    db.session.commit()
    return redirect("/")

@app.route("/delete", methods=["POST"])
def delete_subject():
    title = request.form.get("title")
    subject = Subject.query.filter_by(title=title).first()
    db.session.delete(subject)
    db.session.commit()
    return redirect("/")
