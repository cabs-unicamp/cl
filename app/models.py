from app import app, db
import datetime as dt
from datetime import datetime

students_courses = db.Table('students_courses',
    db.Column('student_id', db.Integer, db.ForeignKey('students.id'), primary_key=True),
    db.Column('course_id', db.Integer, db.ForeignKey('courses.id'), primary_key=True)
)

class NumericPrimaryIdMixin(object):
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)


class TimeStampMixin(object):
    created_at = db.Column(db.DateTime, default=datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now)


class User(db.Model, NumericPrimaryIdMixin, TimeStampMixin):

    __tablename__ = "users"

    username = db.Column(db.String(255), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    # person = db.relationship("Person", uselist=False, back_populates="user")


    @property
    def password(self):
        """Prevents access to password property"""
        raise AttributeError("Password not readable.")

    @password.setter
    def password(self, password):
        """TODO: not implemented yet"""
        self.password_hash = password


class Person(db.Model):

    __tablename__ = "people"

    # user_id=db.Column(db.Integer, db.ForeignKey("users.id"))
    # user=db.relationship("User", back_populates="person")
    first_name = db.Column(db.String(50))
    last_name = db.Column(db.String(50))
    rg=db.Column(db.String(20)) # 9
    cpf=db.Column(db.String(20)) # 11
    phone_prefix=db.Column(db.String(2))
    phone_number=db.Column(db.String(9))
    email_address = db.Column(db.String(255), primary_key=True)
    person_type = db.Column(db.String(50))

    __mapper_args__ = {
            "polymorphic_identity": "person",
            "polymorphic_on": person_type
            }

    def __repr__(self):
        return "<{self.person_type}: {} {} ({})>".format(self.first_name, self.last_name, self.email_address)


class Student(Person, NumericPrimaryIdMixin, TimeStampMixin):

    __tablename__ = "students"

    __mapper_args__ = {
        "polymorphic_identity": "students",
    }

    email_address = db.Column(db.String(255), db.ForeignKey('people.email_address'))
    courses = db.relationship("Course", secondary=students_courses, back_populates="students")

    def __repr__(self):
        return "<Student ID: {}>".format(self.id)


class Teacher(Person, NumericPrimaryIdMixin, TimeStampMixin):

    __tablename__ = "teachers"

    email_address = db.Column(db.String(255), db.ForeignKey('people.email_address'))
    courses = db.relationship("Course", back_populates="teacher")

    __mapper_args__ = {
        "polymorphic_identity": "teachers",
    }

    def __repr__(self):
        return "<Staff ID: {}>".format(self.id)


class Subject(db.Model, TimeStampMixin, NumericPrimaryIdMixin):

    __tablename__ = 'subjects'

    title = db.Column(db.String(50))

    def __repr__(self):
        return "<Subject ID: {} ({})>".format(self.id, self.title)


class Semester(db.Model, TimeStampMixin, NumericPrimaryIdMixin):

    __tablename__ = 'semesters'

    code = db.Column(db.String(10))
    current = db.Column(db.Boolean)
    courses = db.relationship("Course", back_populates="semester")

    def __repr__(self):
        return "<Semester ID: {}>".format(self.id)


class Course(db.Model, TimeStampMixin, NumericPrimaryIdMixin):

    __tablename__ = 'courses'

    title = db.Column(db.String(50))
    description = db.Column(db.String(150))

    semester_id = db.Column(db.Integer, db.ForeignKey("semesters.id"))
    semester = db.relationship("Semester", back_populates="courses")

    subject_id = db.Column(db.Integer, db.ForeignKey("subjects.id"))
    subject = db.relationship("Subject")

    teacher_id = db.Column(db.Integer, db.ForeignKey("teachers.id"))
    teacher = db.relationship("Teacher", back_populates="courses")

    students = db.relationship("Student", secondary=students_courses, back_populates='courses')

    def __repr__(self):
        return "<Course ID: {} ({})>".format(self.id, self.title)


