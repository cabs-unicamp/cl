from marshmallow import fields, post_load
from app import ma
from app.models import User, Subject, Course, Semester, Student, Teacher

""" Marshmallow schemas for de/serialization """

class TeacherSchema(ma.SQLAlchemySchema):

    class Meta:
        model = Teacher

    id=ma.auto_field()
    first_name=ma.auto_field(reqired=True)
    last_name=ma.auto_field(required=True)
    email_address=fields.Email(required=True)
    rg=ma.auto_field(required=True)
    cpf=ma.auto_field(required=True)
    phone_prefix=ma.auto_field(required=True)
    phone_number=ma.auto_field(required=True)
    courses=ma.auto_field()

    @post_load
    def make_teacher(self, data, **kwargs):
        return Teacher(**data)


class SubjectSchema(ma.SQLAlchemySchema):

    class Meta:
        model = Subject

    id=ma.auto_field()
    title = ma.auto_field(required=True)

    @post_load
    def make_subject(self, data, **kwargs):
        return Subject(**data)


class SemesterSchema(ma.SQLAlchemySchema):

    class Meta:
        model = Semester

    id=ma.auto_field()
    code = ma.auto_field(required=True)
    current = ma.auto_field(required=True)
    courses = ma.auto_field()

    @post_load
    def make_semester(self, data, **kwargs):
        return Semester(**data)


class CourseSchema(ma.SQLAlchemySchema):

    class Meta:
        model = Course

    id=ma.auto_field()
    title=ma.auto_field(required=True)
    subject_id=ma.auto_field(required=True)
    subject=ma.auto_field()
    semester_id=ma.auto_field(required=True)
    semester=ma.auto_field()
    teacher_id=ma.auto_field(required=True)
    teacher=ma.auto_field()
    description=ma.auto_field()
    students=ma.auto_field()

    @post_load
    def make_course(self, data, **kwargs):
        return Course(**data)


class StudentSchema(ma.SQLAlchemySchema):

    class Meta:
        model = Student

    id=ma.auto_field()
    first_name=ma.auto_field(reqired=True)
    last_name=ma.auto_field(required=True)
    email_address=ma.auto_field(required=True)
    rg=ma.auto_field(required=True)
    cpf=ma.auto_field(required=True)
    phone_prefix=ma.auto_field(required=True)
    phone_number=ma.auto_field(required=True)
    courses=ma.auto_field()

    @post_load
    def make_student(self, data, **kwargs):
        return Student(**data)


subject_schema = SubjectSchema()
subjects_schema = SubjectSchema(many=True)

semester_schema = SemesterSchema()
semesters_schema = SemesterSchema(many=True)

course_schema = CourseSchema()
courses_schema = CourseSchema(many=True)

teacher_schema = TeacherSchema()
teachers_schema = TeacherSchema(many=True)

student_schema = StudentSchema()
students_schema = StudentSchema(many=True)
