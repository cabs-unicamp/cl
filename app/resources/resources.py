from flask import request
import random
from flask_restful import Resource, reqparse
from app.schemas import subject_schema, subjects_schema, semester_schema, semesters_schema, course_schema, courses_schema, teacher_schema, teachers_schema, student_schema, students_schema
from app.models import Subject, Course, Semester, Teacher, Student
from app import api, db

class SubjectListResource(Resource):
    """List of subjects
    URL: /api/subjects/
    Request methods: GET, POST"""

    def get(self):
        subjects = Subject.query.all()
        return subjects_schema.dump(subjects)

    def post(self):
        form_data = request.get_json()
        errors = subject_schema.validate(form_data)
        if errors:
            return {"error": "Missing or incorrect data sent."}, 500
        else:
            new_subject = subject_schema.load(form_data)
            db.session.add(new_subject)
            db.session.commit()
            return subject_schema.dump(new_subject)


class SubjectResource(Resource):
    """Individual subjects
    URL: /api/subjects/<id>
    Request methods: GET, PATCH, DELETE"""

    def get(self, id):
        subject = Subject.query.get_or_404(id)
        return subject_schema.dump(subject)

    def put(self, id):
        subject = subject_schema.dump(Subject.query.get_or_404(id))
        form_data = request.get_json()
        for k, v in form_data.items():
            subject[k] = v
        errors = subject_schema.validate(form_data)
        if errors:
            return {"error": "Missing or incorrect data sent."}, 500
        else:
            new_subject = subject_schema.load(form_data)
            db.session.commit()
            return subject_schema.dump(subject)

    def delete(self, id):
        subject = Subject.query.get_or_404(id)
        db.session.delete(subject)
        db.session.commit()
        return '', 204


class CourseListResource(Resource):
    """List of courses
    URL: /api/courses/
    Request methods: GET, POST"""

    def get(self):
        courses = Course.query.all()
        return courses_schema.dump(courses)

    def post(self):
        form_data = request.get_json()
        errors = course_schema.validate(form_data)
        if errors:
            return {"error": "Missing or incorrect data sent."}, 500
        else:
            new_course = course_schema.load(form_data)
            try:
                subject = Subject.query.get_or_404(new_course.subject_id)
                new_course.subject = subject
            except:
                return {"error": "Invalid subject ID."}, 400
            try:
                teacher = Teacher.query.get_or_404(new_course.teacher_id)
                new_course.teacher = teacher
            except:
                return {"error": "Invalid teacher ID."}, 400
            try:
                semester = Subject.query.get_or_404(new_course.semester_id)
                new_course.semester = semester
            except:
                return {"error": "Invalid teacher ID."}, 400
            db.session.add(new_course)
            db.session.commit()
            return course_schema.dump(new_course)

class CourseResource(Resource):
    """List of courses
    URL: /api/courses/
    Request methods: GET, POST"""

    def get(self, id):
        course = Course.query.get_or_404(id)
        return course_schema.dump(course)

class SemesterListResource(Resource):
    """List of classes
    URL: /api/classes/
    Request methods: GET, POST"""

    def get(self):
        semesters = Semester.query.all()
        return semesters_schema.dump(semesters)

    def post(self):
        form_data = request.get_json()
        errors = semester_schema.validate(form_data)
        if errors:
            return {"message": "Missing or incorrect data sent."}, 500
        else:
            new_semester = semester_schema.load(form_data)
            db.session.add(new_semester)
            db.session.commit()
            return semester_schema.dump(new_semester)


class SemesterResource(Resource):

    def get(self, id):
        semester = Semester.query.get_or_404(id)
        return semester_schema.dump(semester)


class TeacherListResource(Resource):
    """List of teachers
    URL: /api/teachers/
    Request methods: GET, POST"""

    def get(self):
        teachers = Teacher.query.all()
        return teachers_schema.dump(teachers)

    def post(self):
        form_data = request.get_json()
        errors = teacher_schema.validate(form_data)
        if errors:
            return {"error": "Missing or incorrect data sent.{}"}, 500
        else:
            new_teacher = teacher_schema.load(form_data)
            db.session.add(new_teacher)
            db.session.commit()
            return teacher_schema.dump(new_teacher)


class TeacherResource(Resource):

    def get(self, id):
        teacher = Teacher.query.get_or_404(id)
        return teacher_schema.dump(teacher)


class StudentListResource(Resource):

    def get(self):
        students = Student.query.all()
        return students_schema.dump(students)

    def post(self):
        new_student = Student(
                email_address=request.json['email_address'],
                first_name=request.json['first_name'],
                last_name=request.json['last_name'],
                )
        db.session.add(new_student)
        db.session.commit()
        return student_schema.dump(new_student)


class StudentResource(Resource):

    def get(self, id):
        student = Student.query.get_or_404(id)
        return student_schema.dump(student)


