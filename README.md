# cl

# References

 - [Simple CRUD with Flask and SQLAlchemy](https://www.codementor.io/@garethdwyer/building-a-crud-application-with-flask-and-sqlalchemy-dm3wv7yu2)
 - [Flask school app and API](https://github.com/mbithenzomo/flask-school-app-and-api/)
 - [REST API with Flask and SQLAlchemy](https://rahmanfadhil.com/flask-rest-api/)
